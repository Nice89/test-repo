package page.confluance;

import elements.widget.TextInput;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page.AbstractPage;

/**
 * Created by bzawa on 1/13/2017.
 */
public class LoginPage extends AbstractPage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="username")
    private TextInput usernameInput;

    @FindBy(id="password")
    private TextInput passwordInput;

    @FindBy(id="login")
    private WebElement loginButton;

    public void login(String username, String password){
        usernameInput.set(username);
        passwordInput.set(password);
        loginButton.click();
    }
}

package page.confluance;

import elements.widget.TextInput;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import page.AbstractPage;

/**
 * Created by bzawa on 1/16/2017.
 */
public class ContentBodyEditPage extends AbstractPage{

    public ContentBodyEditPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "content-title")
    private TextInput contentTitle;

    public void enterContentTitle(String title){
        customWaits.waitForElementPresent(contentTitle);
        contentTitle.set(title);
    }
}

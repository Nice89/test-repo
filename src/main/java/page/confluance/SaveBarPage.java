package page.confluance;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page.AbstractPage;

/**
 * Created by bzawa on 1/16/2017.
 */
public class SaveBarPage extends AbstractPage {

    public SaveBarPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "rte-button-publish")
    private WebElement saveButton;

    public void savePage(){
        customWaits.waitForElementBeingClickable(saveButton);
        saveButton.click();
    }

}

package page.confluance;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page.AbstractPage;

/**
 * Created by bzawa on 1/13/2017.
 */
public class MenuBarPage extends AbstractPage {

    public MenuBarPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="quick-create-page-button")
    private WebElement createButton;

    public void clickCreateButton(){
        customWaits.waitForElementBeingClickable(createButton);
        createButton.click();
    }

}

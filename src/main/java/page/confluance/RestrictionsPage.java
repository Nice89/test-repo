package page.confluance;

import enums.Restrictions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page.AbstractPage;

/**
 * Created by bzawa on 1/16/2017.
 */
public class RestrictionsPage extends AbstractPage {

    public RestrictionsPage(WebDriver driver) {
        super(driver);
    }

    private final String restrictionsDialogLocator = "//div[@class='page-restrictions-dialog-top']";
    private final String restrictionOptionLocator = "//div[@class='restrictions-dialog-option']//span[@class='title' and contains(text(),'%s')]";

    @FindBy(xpath = restrictionsDialogLocator)
    private WebElement restrictionsDialogTop;

    @FindBy(id = "s2id_page-restrictions-dialog-selector")
    private WebElement selectRestrictionsDropDown;

    @FindBy(id = "page-restrictions-dialog-save-button")
    private WebElement applyButton;

    public void selectRestrictions(Restrictions restrictions) {
        customWaits.waitForElementPresent(restrictionsDialogTop );
        selectRestrictionsDropDown.click();
        WebElement restrictionOption = driver.findElement(By.xpath(String.format(restrictionOptionLocator, restrictions.getValue())));
        customWaits.waitForElementBeingClickable(restrictionOption);
        restrictionOption.click();
    }

    public void saveRestrictions(){
        applyButton.click();
        customWaits.waitForElementIsNotVisible(By.xpath(restrictionsDialogLocator));
    }
}

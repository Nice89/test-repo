package page.confluance;

import elements.widget.Label;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page.AbstractPage;

/**
 * Created by bzawa on 1/16/2017.
 */
public class ContentBodyPage extends AbstractPage {

    public ContentBodyPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h1[@id='title-text']/a")
    private Label contentTitle;

    @FindBy(id = "content-metadata-page-restrictions")
    private WebElement restrictionsHederIcon;

    public String getContentTitleText() {
        return contentTitle.getText();
    }

    public void clickOnRestrictionsHeaderIcon() {
        restrictionsHederIcon.click();
    }

    public String getRestrictionHeaderIconOriginalTitleAtributeText() {
        return restrictionsHederIcon.getAttribute("original-title");
    }
}

package confluance;

import common.AbstractBaseTest;
import enums.Restrictions;
import org.junit.Before;
import org.junit.Test;
import page.confluance.LoginPage;
import steps.PageSteps;

import java.util.UUID;

/**
 * Created by bzawa on 1/13/2017.
 */
public class PageTest extends AbstractBaseTest{

    private LoginPage loginPage;
    private PageSteps pageSteps;

    private final String username = "user1";
    private final String password = "user1";

    @Before
    public void setUp(){
        loginPage = new LoginPage(driver);
        pageSteps = new PageSteps(driver);
    }

    @Test
    public void shouldCreateNewPageSuccessfully() {
        loginPage.login(username,password);
        pageSteps.createNewPage(UUID.randomUUID().toString());
    }

    @Test
    public void shouldSetRestrictionsOnPage() {
        loginPage.login(username,password);
        pageSteps.createNewPage(UUID.randomUUID().toString());
        pageSteps.setRestrictions(Restrictions.VIEWEDIT);
    }
}
